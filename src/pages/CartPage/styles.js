import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  justify-content: center;
`;

export const ButtonContainer = styled.div`
  position: absolute;
  width: 250px;
  top: 4rem;
  right: 4rem;
  > button {
    margin-top: 0;
  }
`;
