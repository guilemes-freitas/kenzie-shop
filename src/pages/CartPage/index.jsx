import { useDispatch, useSelector } from "react-redux";
import Button from "../../components/Button";
import Cart from "../../components/Cart";
import { finishShopThunk } from "../../store/modules/cart/thunk";
import { Container, ButtonContainer } from "./styles";
import { toast } from "react-toastify";

const CartPage = () => {
  const dispatch = useDispatch();
  const products = useSelector((store) => store.cart);

  const handleClean = () => {
    if (products.length) {
      dispatch(finishShopThunk());
      toast.success("Compra finalizada!");
    } else {
      toast.error("Adicione produtos ao carrinho antes!");
    }
  };
  return (
    <Container>
      <Cart />
      <ButtonContainer>
        <Button onClickFunc={handleClean}>Finalizar Compra</Button>
      </ButtonContainer>
    </Container>
  );
};

export default CartPage;
