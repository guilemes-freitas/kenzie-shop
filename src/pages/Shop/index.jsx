import ProductList from "../../components/ProductList";
import { Container } from "./styles";

const Shop = () => {
  return (
    <Container>
      <ProductList />
    </Container>
  );
};

export default Shop;
