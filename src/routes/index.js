import { Route, Switch } from "react-router";
import Nav from "../components/Nav";
import CartPage from "../pages/CartPage";
import Shop from "../pages/Shop";

function Routes() {
  return (
    <Switch>
      <Route exact path="/">
        <Nav />
        <Shop />
      </Route>
      <Route exact path="/cart">
        <Nav />
        <CartPage />
      </Route>
    </Switch>
  );
}

export default Routes;
