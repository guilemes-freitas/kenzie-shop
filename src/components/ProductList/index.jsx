import Product from "../Product";
import { Container, Products, Title } from "./styles";
import { useSelector } from "react-redux";

const ProductList = () => {
  const products = useSelector((store) => store.products);

  return (
    <Container>
      <Title>Coleção de mangás</Title>
      <Products>
        {products.map((product) => {
          return <Product product={product} key={product.id}></Product>;
        })}
      </Products>
    </Container>
  );
};

export default ProductList;
