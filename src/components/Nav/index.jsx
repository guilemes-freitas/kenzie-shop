import { Link } from "react-router-dom";
import Logo from "../../assets/logo.png";
import { Container, NavContainer, HomeLink } from "./styles";

import { FiShoppingCart } from "react-icons/fi";

import { NavLink } from "./styles";
import { useSelector } from "react-redux";
import { Badge } from "@material-ui/core";

export default function PrimarySearchAppBar() {
  const cart = useSelector((state) => state.cart);

  return (
    <Container>
      <NavContainer>
        <Link to="/">
          <HomeLink>
            <img src={Logo} alt="logo"></img>
            <h5>Kenzie Shop</h5>
          </HomeLink>
        </Link>
      </NavContainer>
      <nav>
        <NavLink to="/cart">
          <Badge badgeContent={cart.length} color="primary">
            <FiShoppingCart size={20} />
          </Badge>
          <span> Carrinho </span>
        </NavLink>
      </nav>
    </Container>
  );
}
