import { useDispatch } from "react-redux";
import {
  addProductThunk,
  removeProductThunk,
} from "../../store/modules/cart/thunk";
import Button from "../Button";
import { ContainerProduct } from "./styles";

const Product = ({ product, isRemovable = false }) => {
  const dispatch = useDispatch();
  const { id, name, priceFormatted, image } = product;

  return (
    <ContainerProduct>
      <img src={image} alt={name}></img>
      <h4>{name}</h4>
      <span>{priceFormatted}</span>
      {isRemovable ? (
        <Button blueSchema onClickFunc={() => dispatch(removeProductThunk(id))}>
          Remover
        </Button>
      ) : (
        <Button onClickFunc={() => dispatch(addProductThunk(product))}>
          Comprar
        </Button>
      )}
    </ContainerProduct>
  );
};

export default Product;
