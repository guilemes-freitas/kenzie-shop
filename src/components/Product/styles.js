import styled from "styled-components";

export const ContainerProduct = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 300px;
  min-width: 200px;
  height: 475px;
  position: relative;
  background-color: var(--white);
  box-shadow: 0px 10px 4px rgb(0 0 0 / 30%);

  > img {
    width: 80%;
    max-height: 343px;
  }
  > button {
    position: absolute;
    bottom: 0;
    width: 80%;
    margin-bottom: 1.5rem;
  }
  > span {
    position: absolute;
    bottom: 0;
    margin-bottom: 4.4rem;
    font-weight: bold;
  }
`;
