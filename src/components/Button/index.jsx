import { Container } from "./styles";

const Button = ({ onClickFunc, blueSchema = false, children, ...rest }) => {
  return (
    <Container type="button" blueSchema={blueSchema} onClick={onClickFunc}>
      {children}
    </Container>
  );
};

export default Button;
