import { useSelector } from "react-redux";
import formatValue from "../../utils/formatValue";
import Product from "../Product";
import { Container, CartContainer, CartInfo } from "./styles";

const Cart = () => {
  const cart = useSelector((store) => store.cart);

  return (
    <Container>
      <CartInfo>
        <h1>Meu carrinho de compras</h1>
        <label>Total - </label>
        <label>
          {formatValue(cart.reduce((acc, product) => acc + product.price, 0))}
        </label>
      </CartInfo>
      <CartContainer>
        {cart.map((product) => (
          <Product key={product.name} product={product} isRemovable />
        ))}
      </CartContainer>
    </Container>
  );
};

export default Cart;
