import formatValue from "../../../utils/formatValue";

const defaultState = [
  {
    id: 0,
    name: "Berserk Vol. 1",
    price: 19.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/91oSUA0bSuL.jpg",
  },
  {
    id: 1,
    name: "Berserk Vol. 2",
    price: 19.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/91C7rVvxP-L.jpg",
  },
  {
    id: 2,
    name: "Berserk Vol. 4",
    price: 19.0,
    image: "https://images-na.ssl-images-amazon.com/images/I/918LBgsEesL.jpg",
  },
  {
    id: 3,
    name: "Vagabond Vol. 1",
    price: 17.9,
    image: "https://images-na.ssl-images-amazon.com/images/I/51ZIlvl3E0L.jpg",
  },
  {
    id: 4,
    name: "Vagabond Vol. 2",
    price: 17.9,
    image: "https://images-na.ssl-images-amazon.com/images/I/91WX+eMJ2ZL.jpg",
  },
  {
    id: 5,
    name: "Vagabond Vol. 3",
    price: 17.9,
    image: "https://images-na.ssl-images-amazon.com/images/I/91KDm4kUI2L.jpg",
  },
  {
    id: 6,
    name: "Vagabond Bundle Vol. 1-10",
    price: 179.0,
    image: "https://img.assinaja.com/assets/tZ/004/img/124378_520x520.jpg",
  },
  {
    id: 7,
    name: "Junji Ito Fragmentos do Horror",
    price: 54.9,
    image:
      "https://a-static.mlcdn.com.br/1500x1500/livro-fragmentos-do-horror/magazineluiza/226017800/d2b8b5f866b92d409438933f69b5a309.jpg",
  },
  {
    id: 8,
    name: "Monster Kanzenban - 01",
    price: 79.9,
    image: "https://img.assinaja.com/assets/tZ/004/img/186778_520x520.jpg",
  },
  {
    id: 9,
    name: "Vinland Saga Vol. 1",
    price: 44.9,
    image:
      "http://pointnerd20.com.br/wp-content/uploads/2020/07/EUpLmGdXkAMsAl5.jpg",
  },
  {
    id: 10,
    name: "Vinland Saga Vol. 2",
    price: 44.9,
    image: "https://img.assinaja.com/assets/tZ/004/img/220046_520x520.jpg",
  },
];

const productsReducer = (state = defaultState, action) => {
  state = state.sort((current, next) => {
    if (current.name < next.name) {
      return -1;
    }
    if (current.name > next.name) {
      return 1;
    }

    return 0;
  });

  state = state.map((product) => ({
    ...product,
    priceFormatted: formatValue(product.price),
  }));
  return state;
};

export default productsReducer;
