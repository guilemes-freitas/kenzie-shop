import { addProduct, removeProduct, finishShop } from "./actions";

export const addProductThunk = (product) => {
  return (dispatch) => {
    const list = JSON.parse(localStorage.getItem("@KenzieShop:Cart")) || [];
    list.push(product);
    console.log(list);
    localStorage.setItem("@KenzieShop:Cart", JSON.stringify(list));
    dispatch(addProduct(product));
  };
};

export const removeProductThunk = (id) => {
  return (dispatch, getStore) => {
    console.log(id);
    const { cart } = getStore();
    console.log("mid", cart);
    const list = cart.filter((product) => product.id !== id);
    localStorage.setItem("@KenzieShop:Cart", JSON.stringify(list));
    dispatch(removeProduct(list));
  };
};

export const finishShopThunk = (list = []) => {
  return (dispatch) => {
    localStorage.clear();
    dispatch(finishShop(list));
  };
};
