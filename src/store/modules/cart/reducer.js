import { ADD_PRODUCT, REMOVE_PRODUCT, FINISH_SHOP } from "./actionsTypes";
const initialState = JSON.parse(localStorage.getItem("@KenzieShop:Cart")) || [];
const cartReducer = (state = initialState, action) => {
  const { list } = action;
  switch (action.type) {
    case ADD_PRODUCT:
      const { product } = action;
      return [...state, product];

    case REMOVE_PRODUCT:
      return list;

    case FINISH_SHOP:
      return list;
    default:
      return state;
  }
};

export default cartReducer;
