import { ADD_PRODUCT, REMOVE_PRODUCT, FINISH_SHOP } from "./actionsTypes";

export const addProduct = (product) => ({
  type: ADD_PRODUCT,
  product,
});

export const removeProduct = (list) => ({
  type: REMOVE_PRODUCT,
  list,
});

export const finishShop = (list) => ({
  type: FINISH_SHOP,
  list,
});
