export const ADD_PRODUCT = "@cart/ADD";
export const REMOVE_PRODUCT = "@cart/REMOVE";
export const FINISH_SHOP = "@cart/FINISH";
